package com.life.alarmtest.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.List;

@JsonTypeName("NodeAlarm")
public class NodeAlarm extends AbstractAlarmPayload {
    private List<String> brokenNodeList;

    public NodeAlarm(List<String> nodeAddressList) {
        this.brokenNodeList = nodeAddressList;
    }

    public NodeAlarm() {
    }

    public List<String> getBrokenNodeList() {
        return brokenNodeList;
    }

    public void setBrokenNodeList(List<String> brokenNodeList) {
        this.brokenNodeList = brokenNodeList;
    }

    @Override
    public String toString() {
        return "NodeAlarm{" +
                "brokenNodeList=" + brokenNodeList +
                '}';
    }
}
