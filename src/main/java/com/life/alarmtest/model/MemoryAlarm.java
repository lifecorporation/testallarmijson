package com.life.alarmtest.model;

import com.fasterxml.jackson.annotation.JsonTypeName;


@JsonTypeName("MemoryAlarm")
public class MemoryAlarm extends AbstractAlarmPayload {
    private int currentMemory;
    private int availableMemory;
    private int sessionsNumber;

    public MemoryAlarm(int currentMemory, int availableMemory, int sessionsNumber) {
        this.currentMemory = currentMemory;
        this.availableMemory = availableMemory;
        this.sessionsNumber = sessionsNumber;
    }

    public MemoryAlarm() {
    }

    public int getCurrentMemory() {
        return currentMemory;
    }

    public void setCurrentMemory(int currentMemory) {
        this.currentMemory = currentMemory;
    }

    public int getAvailableMemory() {
        return availableMemory;
    }

    public void setAvailableMemory(int availableMemory) {
        this.availableMemory = availableMemory;
    }

    public int getSessionsNumber() {
        return sessionsNumber;
    }

    public void setSessionsNumber(int sessionsNumber) {
        this.sessionsNumber = sessionsNumber;
    }

    @Override
    public String toString() {
        return "MemoryAlarm{" +
                "currentMemory=" + currentMemory +
                ", availableMemory=" + availableMemory +
                ", sessionsNumber=" + sessionsNumber +
                '}';
    }
}
