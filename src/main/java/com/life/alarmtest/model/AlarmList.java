package com.life.alarmtest.model;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.*;

public class AlarmList {
    private List<Alarm> list;

    public AlarmList(ArrayList<Alarm> list) {
        this.list = list;
    }

    public AlarmList() {
        list = new ArrayList<>();
    }

    public List<Alarm> getList() {
        return list;
    }

    public void setList(List<Alarm> list) {
        this.list = list;
    }

    private void addAlarm(Alarm alarm) {
        list.add(alarm);
    }

    public static String buildFakeListAsJson() throws JsonProcessingException {
        AlarmList list = new AlarmList();

        list.addAlarm(new Alarm(new Date(), UUID.randomUUID(), new BatteryAlarm(10)));
        list.addAlarm(new Alarm(new Date(), UUID.randomUUID(), new NodeAlarm(Arrays.asList("NODE_1", "NODE_2"))));
        list.addAlarm(new Alarm(new Date(), UUID.randomUUID(), new MemoryAlarm(50, 100, 32)));
        
        return JsonObjectMapperBuilder.build().writeValueAsString(list);
    }
}
