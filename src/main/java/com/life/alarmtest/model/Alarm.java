package com.life.alarmtest.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Date;
import java.util.UUID;

public class Alarm {

    private Date timestamp;
    private UUID uuid;

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
            property = "alarm_payload_id",
            visible = true,
            include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
    @JsonSubTypes({
            @JsonSubTypes.Type(value = BatteryAlarm.class, name = "BatteryAlarm"),
            @JsonSubTypes.Type(value = MemoryAlarm.class, name = "MemoryAlarm"),
            @JsonSubTypes.Type(value = NodeAlarm.class, name = "NodeAlarm"),
    })
    private AbstractAlarmPayload alarmPayload;

    public Alarm(Date timestamp, UUID uuid, AbstractAlarmPayload abstractAlarmPayload) {
        this.timestamp = timestamp;
        this.uuid = uuid;
        this.alarmPayload = abstractAlarmPayload;
    }

    // no args costructor, getter and setters

    public Alarm() {
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public AbstractAlarmPayload getAlarmPayload() {
        return alarmPayload;
    }

    public void setAlarmPayload(AbstractAlarmPayload alarmPayload) {
        this.alarmPayload = alarmPayload;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "timestamp=" + timestamp +
                ", uuid=" + uuid +
                ", alarmPayload=" + alarmPayload +
                '}';
    }
}
