package com.life.alarmtest.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.jsontype.DefaultBaseTypeLimitingValidator;

public class JsonObjectMapperBuilder {
    public static ObjectMapper build() {

        return JsonMapper.builder()
                .configure(SerializationFeature.INDENT_OUTPUT, true)
                //.activateDefaultTyping(new DefaultBaseTypeLimitingValidator(), ObjectMapper.DefaultTyping.NON_FINAL)
                .build();
    }
}
