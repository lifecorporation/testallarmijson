package com.life.alarmtest.model;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("BatteryAlarm")
public class BatteryAlarm extends AbstractAlarmPayload {
    private int batteryLevel;

    public BatteryAlarm(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public BatteryAlarm() {
    }

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    @Override
    public String toString() {
        return "BatteryAlarm{" +
                "batteryLevel=" + batteryLevel +
                '}';
    }
}
