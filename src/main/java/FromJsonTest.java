import com.fasterxml.jackson.core.JsonProcessingException;
import com.life.alarmtest.model.AlarmList;
import com.life.alarmtest.model.JsonObjectMapperBuilder;

public class FromJsonTest {
    public static void main(String[] args) throws JsonProcessingException {

        System.out.println("Creating fake alarm list....");

        String jsonString = AlarmList.buildFakeListAsJson();

        System.out.println("Fake allarm list is:\n" + jsonString);

        AlarmList alarmList = JsonObjectMapperBuilder.build().readValue(jsonString, AlarmList.class);

        System.out.println("Alarm list size " + alarmList.getList().size());

        System.out.println("First alarm is " + alarmList.getList().get(0).toString());
    }
}
